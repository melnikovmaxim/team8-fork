﻿using System.Data;
using FluentValidation;

namespace Shop.API.Mediatr.Queries
{
    public class RandomNumbersQueryValidator: AbstractValidator<RandomNumbersQuery>
    {
        public RandomNumbersQueryValidator()
        {
            RuleFor(x => x.Count)
                .LessThan(11)
                .WithMessage("Количество рандомных чисел должно быть 10 или меньше")
                .GreaterThan(0)
                .WithMessage("Количество рандомных чисел должно быть 1 или больше");
        }
    }
}