﻿using System.Collections;
using System.Collections.Generic;
using MediatR;

namespace Shop.API.Mediatr.Queries
{
    public record RandomNumbersQuery(int Count) : IRequest<IEnumerable<int>>;
}