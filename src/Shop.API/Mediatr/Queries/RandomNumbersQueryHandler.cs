﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Shop.API.Mediatr.Queries
{
    public class RandomNumbersQueryHandler: IRequestHandler<RandomNumbersQuery, IEnumerable<int>>
    {
        private Random _random;
        
        public RandomNumbersQueryHandler()
        {
            _random = new Random();
        }
        
        public Task<IEnumerable<int>> Handle(RandomNumbersQuery request, CancellationToken cancellationToken)
        {
            var randomNumbers = Enumerable.Range(0, request.Count)
                                          .Select(x => _random.Next());

            return Task.FromResult(randomNumbers);
        }
    }
}