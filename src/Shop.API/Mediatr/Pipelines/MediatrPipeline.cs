﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;

namespace Shop.API.Mediatr.Pipelines
{
    public class MediatrPipeline<TRequest, TResponse>: IPipelineBehavior<TRequest, TResponse>
        where TRequest: IRequest<TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public MediatrPipeline(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            // Валидация в пайплайне, например если query/command создали через new и кидаем в медиатор
            var validationContext = new ValidationContext<TRequest>(request);
            var validationFailures = _validators
                .Select(x => x.Validate(validationContext))
                .SelectMany(x => x.Errors)
                .Where(x => x is not null)
                .ToList();

            if (validationFailures.Any())
            {
                var errors = validationFailures.Select(x => x.ErrorMessage);

                throw new ValidationException(string.Join(", ", errors));
            }

            var response = await next().ConfigureAwait(false);

            return response;
        }
    }
}