﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Shop.API.Mediatr.Queries;

namespace Shop.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController: ControllerBase
    {
        private readonly IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("random")] // post только для примера с RandomNumbersQuery
        public async Task<IActionResult> GetRandomNumbers([FromBody] RandomNumbersQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }
    }
}